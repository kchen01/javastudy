package com.company;

public class Math {

    public Math() {
    }

    //Adds up the positive integers from 0 to the input number.
    public int Sum(int n) {
        int total = 0;
        for (int i = 0; i <= n; i++) {
            total += i;
        }
        return total;
    }

    public boolean isPrime( int pNum ) {
        for( int idx=2; idx<pNum; idx++ ) {
            if( pNum % idx == 0 ) {
                System.out.println( "Divided by " + idx );
                return false;
            }
        }
        return true;
    }

    public boolean isPrimeNumber(double n) {
        for (double i = 2; i < n; i++) {
            if ((n / i) == (int) (n / i)) {
                return false;
            }
        }
        return true;
    }

}



