Kevin's Java study code.

# Installation of Tools

1. Install __Homebrew__  
    https://brew.sh/
1. IntelliJ IDEA  
    [IntelliJ IDEA: The Java IDE for Professional Developers by JetBrains](https://www.jetbrains.com/idea/)
    ```
    $ brew search IDEA
        ==> Casks
        homebrew/cask/intellij-idea        homebrew/cask/intellij-idea-ce
    $ brew cask install intellij-idea-ce
    ```

